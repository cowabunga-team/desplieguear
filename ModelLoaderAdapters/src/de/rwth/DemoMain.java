package de.rwth;

import system.ArActivity;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;

public class DemoMain extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		LinearLayout l = new LinearLayout(this);
		l.addView(newButton("data/models/sphere.obj",null));
		l.addView(newButton("data/models/pizza_V2_4.obj", "data/models/pizzapepperoni4.jpg"));
		l.setOrientation(LinearLayout.VERTICAL);
		setContentView(l);
	}

	private View newButton(final String fileName, final String textureName) {
		Button b = new Button(this);
		b.setText("Load " + fileName);
		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				ArActivity.startWithSetup(DemoMain.this, new ModelLoaderSetup(
						fileName, textureName));
			}
		});
		return b;
	}

}